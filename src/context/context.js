import React, { useState, useEffect } from 'react';
import mockUser from './mockData.js/mockUser';
import mockRepos from './mockData.js/mockRepos';
import mockFollowers from './mockData.js/mockFollowers';
import axios from 'axios';
import { data } from 'jquery';

const rootUrl = 'https://api.github.com/';

const GithubContext = React.createContext();

const GithubProvider = ({children}) => {
    const [githubUser , setGithubUser] = useState(mockUser);
    const [repos , setRepos] = useState(mockRepos);
    const [followers , setFollwers] = useState(mockFollowers);
    const [limit,setLimit]=useState(0);
    const [error,setError] = React.useState('');
    const fetchLimit = () =>{
        axios(`https://api.github.com/rate_limit`)
        .then((data) =>{
            setLimit(data.data.resources.core.remaining)
        })
        .catch((error)=>{
            console.log(error);
        })}
        
    
    useEffect(()=>{
        fetchLimit();
    },[])
    
    
    
    const [loading,setIsLoading] =useState(false);
    const searchGithubUser = async(user)=>{
        const resp = await axios(`https://api.github.com/users/${user}`)
        .then((data)=>{
            setGithubUser(data.data);
            setError("");
        })
        .catch((error,data)=>{
            console.log(error);
            if(data === undefined){
                setError("this username do not exist")
            }  
        })
        
    }
    const GithubFollowers =async(user)=>{
       const resp = await axios(`https://api.github.com/users/${user}/followers` )
       .then((data)=>{
        setFollwers(data.data)
       })
    
    }
    const ReposInfo = async (user)=>{
        const resp = await axios(`https://api.github.com/users/${user}/repos?per_page=100`)
        .then((data)=>{
            
            setRepos(data.data)
        })
    }
    return (
    <GithubContext.Provider value={{githubUser,error,repos,followers,limit,searchGithubUser,GithubFollowers,ReposInfo}}>
        {children}
    </GithubContext.Provider>)
}

export {GithubProvider,GithubContext};