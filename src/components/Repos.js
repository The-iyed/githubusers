import React from 'react';
import styled from 'styled-components';
import { GithubContext } from '../context/context';
import { ExampleChart, Pie3D, Column3D, Bar3D, Doughnut2D } from './Charts';
const Repos = () => {
  const {repos} = React.useContext(GithubContext);
  let languages = repos.reduce((total,item)=>{
    const {language} = item;
    if(!language) return total
    if (!total[language]) {
      
      total[language] = {label:language , value:1}
    }
    else {
      total[language]={label:language, value:total[language].value+1}
    }
    return total;
  },{})
  let Stars = repos.reduce((totalL,item)=>{
    const  {language,stargazers_count} = item;
    if(!language)return totalL;
    if(!totalL[language]){
      totalL[language] = {label:language , value:stargazers_count}
    }
    else{
      totalL[language] = {label:language , value:totalL[language].value+stargazers_count}
    }
    return totalL
  },{})
  
  let Forks = repos.reduce((tot,item)=>{
    const  {name,forks_count} = item;
    if(!name)return tot;
    if(!tot[name]){
      tot[name] = {label:name , value:forks_count}
    }
    else{
      tot[name] = {label:name , value:tot[name].value+forks_count}
    }
    return tot
  },{})
  let Watchers = repos.reduce((tot,item)=>{
    const  {name,watchers_count} = item;
    if(!name)return tot;
    if(!tot[name]){
      tot[name] = {label:name , value:watchers_count}
    }
    else{
      tot[name] = {label:name , value:tot[name].value+watchers_count}
    }
    let obj=[]
    return tot
  },{})


 

  function sortObjectByValue(obj) {
    let sortable = [];
    let finalOne ={}
    for (let key in obj) {
        if ((obj.hasOwnProperty(key))) {
            sortable.push([key, obj[key].value]);
        }
    }
    sortable.sort(function(a, b) {
        return b[1] - a[1];
    });
    var sortedObj = {};
    sortable.map((el,index)=>{
      if(index<=4){
        let key = el[0];
        let value = el[1];
        sortedObj[key,value] = {label:key,value:value};
      }
    
    })
    
    return sortedObj;
}

 //

  languages = Object.values(languages);
 
  const chartData = [];
  languages.map((language)=>{
    chartData.push(language);
  });
  Stars = Object.values(Stars);
  const startsdata = [];
  Stars.map((stars)=>{
    startsdata.push(stars);
  });
  let TheForks = Object.values(sortObjectByValue(Forks));
  const forkData =[] ;
  TheForks.map((fork)=>{
    forkData.push(fork)
  })
   let TheWatchers = Object.values((sortObjectByValue(Watchers)))
  const watchersData = [];
  TheWatchers.map((watcher)=>{
    watchersData.push(watcher)
  })
  
  
  
 


  return (
    <section className='section'>
      <Wrapper className='section-center'>
        <Pie3D data={chartData}/>
        <Column3D data={watchersData}/>
        <Doughnut2D data={startsdata}/>
        <Bar3D data={forkData}/>
        
      </Wrapper>
    </section>
  );
};

const Wrapper = styled.div`
  display: grid;
  justify-items: center;
  gap: 2rem;
  @media (min-width: 800px) {
    grid-template-columns: 1fr 1fr;
  }

  @media (min-width: 1200px) {
    grid-template-columns: 2fr 3fr;
  }

  div {
    width: 100% !important;
  }
  .fusioncharts-container {
    width: 100% !important;
  }
  svg {
    width: 100% !important;
    border-radius: var(--radius) !important;
  }
`;

export default Repos;
