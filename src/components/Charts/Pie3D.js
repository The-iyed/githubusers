// STEP 1 - Include Dependencies
// Include react
import React from "react";
// Include the react-fusioncharts component
import ReactFC from "react-fusioncharts";

// Include the fusioncharts library
import FusionCharts from "fusioncharts";

// Include the chart type
import Column2D from "fusioncharts/fusioncharts.charts";

// Include the theme as fusion
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

// Adding the chart and theme as dependency to the core fusioncharts
ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme);

// STEP 2 - Chart Data

// STEP 3 - Creating the JSON object to store the chart configurations

const ChartComponent = ({data}) =>{
  const chartConfigs = {
  type: "pie2d",
  width: "90%",
  height: "400", 
  dataFormat: "json", 
  dataSource: {
    chart: {
      decimals:0,
      pieRadius:"35%",
      caption: "Languages",
      subCaption: "In MMbbl = One Million barrels",
      xAxisName: "Country",
      yAxisName: "Reserves (MMbbl)",
      theme: "fusion",
      numberSuffix: "K",
    },
    data,
  }
};
  return <ReactFC {...chartConfigs}/>;
}
// STEP 4 - Creating the DOM element to pass the react-fusioncharts component


export default ChartComponent;









